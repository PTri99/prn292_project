USE [CMS]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Email] [nvarchar](max) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[UserID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryID] [int] NOT NULL,
	[CategoryName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Course]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[CourseID] [int] NOT NULL,
	[CourseName] [nvarchar](max) NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Material]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Material](
	[MaterialID] [int] NOT NULL,
	[Link] [nvarchar](max) NOT NULL,
	[UserID] [int] NOT NULL,
	[SubjectID] [int] NOT NULL,
 CONSTRAINT [PK_Material] PRIMARY KEY CLUSTERED 
(
	[MaterialID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleID] [int] NOT NULL,
	[RoleName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Section]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Section](
	[section] [int] NULL,
	[startdate] [datetime] NULL,
	[enddate] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Subject]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subject](
	[SubjectID] [int] NOT NULL,
	[SubjectName] [nvarchar](max) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[CourseID] [int] NOT NULL,
 CONSTRAINT [PK_Subject] PRIMARY KEY CLUSTERED 
(
	[SubjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Subject_Resource]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subject_Resource](
	[ResourceID] [int] NOT NULL,
	[ResourceName] [nvarchar](max) NOT NULL,
	[UploadDate] [datetime] NOT NULL,
	[SubjectID] [int] NOT NULL,
	[Section] [int] NOT NULL,
 CONSTRAINT [PK_Subject_Resource] PRIMARY KEY CLUSTERED 
(
	[ResourceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Sex] [bit] NOT NULL,
	[DOB] [datetime] NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User_Subject]    Script Date: 11/15/2019 11:15:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Subject](
	[UserID] [int] NOT NULL,
	[SubjectID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[SubjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'phongbthe130725@fpt.edu.vn', N'123', 130725)
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'trihphe130589@fpt.edu.vn', N'123', 130589)
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'hainnhe130585@fpt.edu.vn', N'123', 130585)
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'bachhvhe130503@fpt.edu.vn', N'123', 130503)
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'tiennmhe130489@fpt.edu.vn', N'123', 130489)
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'trangnthe130587@fpt.edu.vn', N'123', 130487)
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'nainnhe130307@fpt.edu.vn', N'123', 130307)
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'bantq@fpt.edu.vn', N'123', 100003)
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'tridt@fpt.edu.vn', N'123', 100002)
INSERT [dbo].[Account] ([Email], [Password], [UserID]) VALUES (N'chilp@fpt.edu.vn', N'123', 100001)
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (1, N'Fundamental')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (2, N'Software Engineering')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (3, N'Bussiness Administration')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (4, N'Master of Software Engineering')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (5, N'Professional Certification')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (6, N'Multimedia Communications')
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (100, N'English', 1)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (101, N'Chinese', 1)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (102, N'Japanese', 1)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (103, N'Phát Triển Cá Nhân', 1)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (104, N'Orientation', 1)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (105, N'Soft Skills', 1)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (106, N'Asian Cultures', 1)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (107, N'Mathematics ', 1)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (200, N'Computing Fundamentals', 2)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (201, N'Software Engineering', 2)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (202, N'Information Technology Specialization', 2)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (203, N'Information Technology Specialization', 2)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (204, N'Electronics and Communications', 2)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (205, N'Computer Science', 2)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (206, N'Graphic Design', 2)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (207, N'Information Assurance', 2)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (300, N'Foundation', 3)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (301, N'Tài liệu hướng dẫn sinh viên học tại FSB', 3)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (302, N'OJT', 3)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (303, N'Graduation', 3)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (304, N'English', 3)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (305, N'Finance, Accounting, Economics and Banking', 3)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (306, N'General management', 3)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (400, N'Specialized knowledge', 4)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (401, N'In-depth knowledge', 4)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (402, N'General knowledge', 4)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (403, N'Subjects for K1,2', 4)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (600, N'Corporate Communication', 6)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (601, N'Fundamentals of Multimedia', 6)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (602, N'Fundamental of Graphic Design', 6)
INSERT [dbo].[Course] ([CourseID], [CourseName], [CategoryID]) VALUES (603, N'Media Writing', 6)
INSERT [dbo].[Material] ([MaterialID], [Link], [UserID], [SubjectID]) VALUES (1, N'testing.jpg', 100001, 1)
INSERT [dbo].[Material] ([MaterialID], [Link], [UserID], [SubjectID]) VALUES (2, N'english.jpg', 100001, 2)
INSERT [dbo].[Material] ([MaterialID], [Link], [UserID], [SubjectID]) VALUES (3, N'trial.jpg', 100001, 3)
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (1, N'Student')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (2, N'Teacher')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (3, N'Admin')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (4, N'Guest')
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (1, CAST(N'2019-11-10 00:00:00.000' AS DateTime), CAST(N'2019-11-17 00:00:00.000' AS DateTime))
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (2, CAST(N'2019-11-18 00:00:00.000' AS DateTime), CAST(N'2019-11-24 00:00:00.000' AS DateTime))
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (3, CAST(N'2019-11-25 00:00:00.000' AS DateTime), CAST(N'2019-12-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (4, CAST(N'2019-12-02 00:00:00.000' AS DateTime), CAST(N'2019-12-08 00:00:00.000' AS DateTime))
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (5, CAST(N'2019-12-09 00:00:00.000' AS DateTime), CAST(N'2019-12-15 00:00:00.000' AS DateTime))
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (6, CAST(N'2019-12-16 00:00:00.000' AS DateTime), CAST(N'2019-12-22 00:00:00.000' AS DateTime))
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (7, CAST(N'2019-12-23 00:00:00.000' AS DateTime), CAST(N'2019-12-29 00:00:00.000' AS DateTime))
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (8, CAST(N'2019-12-30 00:00:00.000' AS DateTime), CAST(N'2020-01-06 00:00:00.000' AS DateTime))
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (9, CAST(N'2020-01-07 00:00:00.000' AS DateTime), CAST(N'2020-01-13 00:00:00.000' AS DateTime))
INSERT [dbo].[Section] ([section], [startdate], [enddate]) VALUES (10, CAST(N'2019-01-14 00:00:00.000' AS DateTime), CAST(N'2019-01-20 00:00:00.000' AS DateTime))
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (1, N'University Success', CAST(N'2019-10-10 00:00:00.000' AS DateTime), CAST(N'2019-12-12 00:00:00.000' AS DateTime), 100)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (2, N'Chinese Elementary', CAST(N'2019-10-10 00:00:00.000' AS DateTime), CAST(N'2019-12-12 00:00:00.000' AS DateTime), 101)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (3, N'Japanese for JLPT', CAST(N'2019-09-09 00:00:00.000' AS DateTime), CAST(N'2019-12-12 00:00:00.000' AS DateTime), 102)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (4, N'Bussiness Writing', CAST(N'2019-10-10 00:00:00.000' AS DateTime), CAST(N'2019-12-12 00:00:00.000' AS DateTime), 100)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (5, N'Chinese Elemetary', CAST(N'2019-10-10 00:00:00.000' AS DateTime), CAST(N'2019-12-12 00:00:00.000' AS DateTime), 101)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (6, N'Japanese Elemantary', CAST(N'2019-08-08 00:00:00.000' AS DateTime), CAST(N'2019-12-12 00:00:00.000' AS DateTime), 102)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (7, N'Modern Physics', CAST(N'2019-08-08 00:00:00.000' AS DateTime), CAST(N'2019-12-12 00:00:00.000' AS DateTime), 107)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (8, N'Introduction to Computing', CAST(N'2019-09-09 00:00:00.000' AS DateTime), CAST(N'2019-12-12 00:00:00.000' AS DateTime), 200)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (9, N'Programming Fundamental', CAST(N'2019-07-07 00:00:00.000' AS DateTime), CAST(N'2019-10-10 00:00:00.000' AS DateTime), 200)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (10, N'Object Oriented', CAST(N'2019-08-08 00:00:00.000' AS DateTime), CAST(N'2019-11-11 00:00:00.000' AS DateTime), 200)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (11, N'Front-end Web', CAST(N'2019-08-08 00:00:00.000' AS DateTime), CAST(N'2019-11-01 00:00:00.000' AS DateTime), 200)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (12, N'Operating System', CAST(N'2019-09-09 00:00:00.000' AS DateTime), CAST(N'2019-11-01 00:00:00.000' AS DateTime), 200)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (13, N'Computing Network', CAST(N'2019-08-01 00:00:00.000' AS DateTime), CAST(N'2019-12-01 00:00:00.000' AS DateTime), 200)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (14, N'Human Computer Interaction', CAST(N'2019-09-01 00:00:00.000' AS DateTime), CAST(N'2019-12-01 00:00:00.000' AS DateTime), 201)
INSERT [dbo].[Subject] ([SubjectID], [SubjectName], [StartDate], [EndDate], [CourseID]) VALUES (15, N'Management', CAST(N'2019-02-01 00:00:00.000' AS DateTime), CAST(N'2019-12-01 00:00:00.000' AS DateTime), 201)
INSERT [dbo].[Subject_Resource] ([ResourceID], [ResourceName], [UploadDate], [SubjectID], [Section]) VALUES (1, N'Test case test', CAST(N'2019-11-11 00:00:00.000' AS DateTime), 1, 1)
INSERT [dbo].[Subject_Resource] ([ResourceID], [ResourceName], [UploadDate], [SubjectID], [Section]) VALUES (2, N'Test case s2', CAST(N'2019-11-12 00:00:00.000' AS DateTime), 1, 1)
INSERT [dbo].[Subject_Resource] ([ResourceID], [ResourceName], [UploadDate], [SubjectID], [Section]) VALUES (3, N'Group ', CAST(N'2019-11-11 00:00:00.000' AS DateTime), 1, 1)
INSERT [dbo].[Subject_Resource] ([ResourceID], [ResourceName], [UploadDate], [SubjectID], [Section]) VALUES (4, N'Ttitle', CAST(N'2019-11-18 00:00:00.000' AS DateTime), 1, 2)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (100001, N'Le Phuong Chi', 0, CAST(N'1980-06-06 00:00:00.000' AS DateTime), N'chilp@fpt.edu.vn', 2)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (100002, N'Tran Dinh Tri', 1, CAST(N'1960-05-24 00:00:00.000' AS DateTime), N'tridt@fpt.edu.vn', 2)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (100003, N'Tran Quy Ban', 1, CAST(N'1990-07-06 00:00:00.000' AS DateTime), N'bantq@fpt.edu.vn', 2)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (130307, N'Nguyen Ngoc Nai', 0, CAST(N'1980-07-03 00:00:00.000' AS DateTime), N'nainnhe130307@fpt.edu.vn', 3)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (130487, N'Nguyen Thu Trang', 0, CAST(N'1998-05-24 00:00:00.000' AS DateTime), N'trangnthe130587@fpt.edu.vn', 1)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (130489, N'Nguyen Mai Tien', 0, CAST(N'1999-04-09 00:00:00.000' AS DateTime), N'tiennmhe130489@fpt.edu.vn', 1)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (130503, N'Hoang Viet Bach', 1, CAST(N'1999-08-03 00:00:00.000' AS DateTime), N'bachhvhe130503@fpt.edu.vn', 1)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (130585, N'Nguyen Ngoc Hai', 1, CAST(N'1999-08-23 00:00:00.000' AS DateTime), N'hainnhe130585@fpt.edu.vn', 1)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (130589, N'Ha Phuc Tri', 1, CAST(N'1999-09-23 00:00:00.000' AS DateTime), N'trihphe130589@fpt.edu.vn', 1)
INSERT [dbo].[User] ([UserID], [Name], [Sex], [DOB], [Email], [RoleID]) VALUES (130725, N'Bui Tien Phong', 1, CAST(N'1999-02-20 00:00:00.000' AS DateTime), N'phongbthe130725@fpt.edu.vn', 1)
INSERT [dbo].[User_Subject] ([UserID], [SubjectID]) VALUES (100001, 1)
INSERT [dbo].[User_Subject] ([UserID], [SubjectID]) VALUES (100002, 2)
INSERT [dbo].[User_Subject] ([UserID], [SubjectID]) VALUES (100003, 3)
ALTER TABLE [dbo].[Account]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Categories] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([CategoryID])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Categories]
GO
ALTER TABLE [dbo].[Material]  WITH CHECK ADD  CONSTRAINT [FK_Material_User] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([SubjectID])
GO
ALTER TABLE [dbo].[Material] CHECK CONSTRAINT [FK_Material_User]
GO
ALTER TABLE [dbo].[Material]  WITH CHECK ADD  CONSTRAINT [FK_Material_User1] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Material] CHECK CONSTRAINT [FK_Material_User1]
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD  CONSTRAINT [FK_Subject_Course] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Subject] CHECK CONSTRAINT [FK_Subject_Course]
GO
ALTER TABLE [dbo].[Subject_Resource]  WITH CHECK ADD  CONSTRAINT [FK_Subject_Resource_Subject] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([SubjectID])
GO
ALTER TABLE [dbo].[Subject_Resource] CHECK CONSTRAINT [FK_Subject_Resource_Subject]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([RoleID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
ALTER TABLE [dbo].[User_Subject]  WITH CHECK ADD  CONSTRAINT [FK_User_Subject_Subject] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([SubjectID])
GO
ALTER TABLE [dbo].[User_Subject] CHECK CONSTRAINT [FK_User_Subject_Subject]
GO
ALTER TABLE [dbo].[User_Subject]  WITH CHECK ADD  CONSTRAINT [FK_User_Subject_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[User_Subject] CHECK CONSTRAINT [FK_User_Subject_User]
GO
