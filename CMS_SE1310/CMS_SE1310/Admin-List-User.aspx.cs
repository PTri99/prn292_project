﻿using CMS_SE1310.App_Code.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_SE1310
{
    public partial class Admin_List_User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session.Contents["Name"] == null)
            {
                Response.Redirect("Login.aspx");
                //Kiểm tra có phải role admin

            }
            else
            {
                if (Session.Contents["Role"].ToString() != "Admin")
                {
                    Response.Redirect("HomeALogin.aspx");
                }
            }
            lbName.Text = Session.Contents["Name"].ToString();
            GridViewUser.EnableViewState = true;
            if (!IsPostBack)
            {

                loadUser();
                loadDDLpopup();
            }
            setUpGridView();
            loadGridView();

        }

        //private void loadGridViewFirst()
        //{
        //    GridViewUser.DataSource = null;
        //    GridViewUser.DataSource = GetAllUser();
        //    GridViewUser.DataBind();
        //}
        private void loadGridView()
        {
            switch (DropDownList1.SelectedIndex)
            {
                case 0:
                    {

                        GridViewUser.DataSource = null;
                        GridViewUser.DataSource = GetAllUser();
                        GridViewUser.DataBind();

                        break;
                    }
                default:
                    {
                        GridViewUser.DataSource = null;
                        GridViewUser.DataSource = GetAllUserByRoleID(DropDownList1.SelectedValue.ToString());
                        GridViewUser.DataBind();

                        break;
                    }


            }
        }
        private void loadUser()
        {
            DropDownList1.DataTextField = "Name";
            DropDownList1.DataValueField = "RoleID";

            DropDownList1.DataSource = null;
            DropDownList1.DataSource = GetAllRole();
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("Select All", "0"));

        }
        private void loadDDLpopup()
        {
            DropDownList3.DataTextField = "Name";
            DropDownList3.DataValueField = "RoleID";

            DropDownList3.DataSource = null;
            DropDownList3.DataSource = GetAllRole();
            DropDownList3.DataBind();

        }
        public List<User> GetAllUser()
        {
            DataProvider dp = new DataProvider();
            DataTable dt = dp.GET_LIST_OBJECT("select UserID, Name, Sex,DOB,Email,RoleName from [User], Role where [User].RoleID = Role.RoleID", null);

            List<User> listDate = new List<User>();
            foreach (DataRow dr in dt.Rows)
            {
                int userID = Convert.ToInt32(dr["userID"]);
                string name = (string)dr["Name"];
                bool temp = (bool)dr["Sex"];
                string sex = "";
                if (temp == true) sex = "Male";
                else sex = "Female";
                string email = (string)dr["Email"];
                string dob = Convert.ToDateTime(dr["DOB"]).ToString("dd/MM/yyyy");
                string rolename = (string)dr["RoleName"];


                listDate.Add(new User(userID, name, sex, dob, email, rolename));




            }
            return listDate;
        }
        public List<User> GetAllUserByRoleID(string roleID)
        {
            DataProvider dp = new DataProvider();
            DataTable dt = dp.GET_LIST_OBJECT("select UserID, Name, Sex,DOB,Email,RoleName from [User], Role where [User].RoleID = Role.RoleID and Role.RoleID = @param1 ", new object[] { roleID });

            List<User> listDate = new List<User>();
            foreach (DataRow dr in dt.Rows)
            {
                int userID = Convert.ToInt32(dr["userID"]);
                string name = (string)dr["Name"];
                bool temp = (bool)dr["Sex"];
                string sex = "";
                if (temp == true) sex = "Male";
                else sex = "Female";
                string email = (string)dr["Email"];
                string dob = Convert.ToDateTime(dr["DOB"]).ToString("dd/MM/yyyy");
                string rolename = (string)dr["RoleName"];


                listDate.Add(new User(userID, name, sex, dob, email, rolename));




            }
            return listDate;
        }
        public List<Role> GetAllRole()
        {
            DataProvider dp = new DataProvider();
            DataTable dt = dp.GET_LIST_OBJECT("select * from Role", null);

            List<Role> listDate = new List<Role>();
            foreach (DataRow dr in dt.Rows)
            {
                int roleID = Convert.ToInt32(dr["RoleID"]);
                string rolename = (string)dr["RoleName"];



                listDate.Add(new Role(roleID, rolename));




            }
            return listDate;
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadGridView();
        }
        void setUpGridView()
        {
            GridViewUser.AutoGenerateColumns = false;
            GridViewUser.Columns.Clear();

            GridViewUser.Columns.Add(new BoundField() { HeaderText = "ID", DataField = "UserID" });
            GridViewUser.Columns.Add(new BoundField() { HeaderText = "Name", DataField = "Name" });
            GridViewUser.Columns.Add(new BoundField() { HeaderText = "Sex", DataField = "Sex" });
            GridViewUser.Columns.Add(new BoundField() { HeaderText = "DOB", DataField = "DOB" });
            GridViewUser.Columns.Add(new BoundField() { HeaderText = "Email", DataField = "Email" });
            GridViewUser.Columns.Add(new BoundField() { HeaderText = "RoleName", DataField = "RoleName" });
            GridViewUser.Columns.Add(new TemplateField() { HeaderText = "Change role" });
           
        }
        protected void GridViewUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {


            if (e.Row.RowIndex < 0) return;
            DropDownList ddListA = new DropDownList();
            //btDel.OnClientClick = "return confirmDel()";
            ddListA.ID = "DropDownList2";
            //ddList.CommandName = e.Row.RowIndex.ToString();
            int colNum = GridViewUser.Columns.Count;

            e.Row.Cells[colNum - 1].Controls.Add(ddListA);

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                var ddList = (DropDownList)e.Row.FindControl("DropDownList2");
                ddList.DataSource = null;
                ddList.DataSource = GetAllRole();

                ddList.DataTextField = "Name";
                ddList.DataValueField = "RoleID";

                ddList.DataBind();

                ddList.Items.Insert(0, new ListItem("Select Role", "0"));

            }
        }



        protected void Save_Click(object sender, EventArgs e)
        {
            DataProvider data = new DataProvider();
            foreach (GridViewRow di in GridViewUser.Rows)
            {
                //if (di.RowIndex < 0) continue;
                DropDownList cb = (DropDownList)di.FindControl("DropDownList2");

                string temp = ((List<User>)GridViewUser.DataSource)[di.RowIndex].RoleName;
                string userid = ((List<User>)GridViewUser.DataSource)[di.RowIndex].UserID.ToString();
                string temp2 = cb.SelectedItem.ToString();
                string newRole = cb.SelectedValue;
                if (cb.SelectedIndex != 0 && temp != temp2)
                {
                    data.ADD_UPDATE_DELETE("update [User] set RoleID = @param1 where UserID = @param2", new object[] { newRole, userid });
                }



            }
            Response.Redirect("Admin-List-User.aspx");
        }


        protected void Add_Click(object sender, EventArgs e)
        {
            DataProvider data = new DataProvider();

            int code = Convert.ToInt32(data.GET_OBJECT("select MAX(UserID) as [count] from [User]", null)["count"].ToString()) + 1;
            string name = nameuser.Text;
            bool sex;
            if (RadioButton1.Checked == true) sex = true;
            else sex = false;
            DateTime dob = Convert.ToDateTime(dobuser.Text);
            string email = emailuser.Text;
            int roleid = Convert.ToInt32(DropDownList3.SelectedValue.ToString());
            int count = Convert.ToInt32(data.GET_OBJECT("select count(*) as [count] from [User] where Email = @param1", new object[] { email })["count"].ToString());
            if (count != 0)
            {
                Response.Write("<script>alert('Email existed.Try again!')</script>");
                return;
            }
           
            data.ADD_UPDATE_DELETE("INSERT INTO [User] VALUES( @param1 , @param2 , @param3 , @param4 , @param5 , @param6 )",
                new object[] { code, name, sex, dob, email, roleid });
            string password = "123";
            data.ADD_UPDATE_DELETE("INSERT INTO [Account] VALUES( @param1 , @param2 , @param3 )",
               new object[] { email, password, code });
            Response.Redirect("Admin-List-User.aspx");




        }




    }
}