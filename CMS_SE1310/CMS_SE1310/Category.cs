﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CMS_SE1310
{
    public class Category
    {
        public int CatID { get; set; }
        public string CatName { get; set; }

        public Category()
        {
        }

        public Category(int catID, string catName)
        {
            CatID = catID;
            CatName = catName;
        }

        public List<Category> ConvertToListCat()
        {
            DataProvider p = new DataProvider();
            DataTable tb = p.GET_LIST_OBJECT("select * from Categories", null);
            List<Category> cateList = new List<Category>();

            foreach (DataRow item in tb.Rows)
            {
                Category c = new Category();
                c.CatID = Convert.ToInt32(item["CategoryID"]);
                c.CatName = item["CategoryName"].ToString();
                //Add to list
                cateList.Add(c);
            }
            return cateList;
        }
    }
}