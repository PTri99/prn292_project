﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubjectDetail.aspx.cs" Inherits="CMS_SE1310.SubjectDetail" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Detail Subject</title>
    <link href="../../CSS/boostrap_4/bootstrap.scss" rel="stylesheet" type="text/css">
    <link href="../../CSS/CustomCSS/SubjectDetail.css" rel="stylesheet" type="text/css">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="CSS/boostrap_4/bootstrap-grid.scss" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../../CSS/sb-admin-2.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <form id="form1" runat="server">

        <!-- Page Wrapper -->

        <div id="wrapper">

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-light bg-white topbar mb-4 static-top shadow">
                        <a href="HomeALogin.aspx">
                            <h1 class="title">eLearning</h1>
                        </a>
                        <div class="float-right">
                            You are signed in as:
                            <asp:Label ID="lbName" runat="server" Text="Label"></asp:Label>
                            <a href="SignOut.aspx" class="btn btn-primary btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-arrow-right"></i>
                                </span>
                                <span class="text">Sign Out</span>
                            </a>
                        </div>
                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-body">
                                <asp:Label CssClass="font-weight-bold titleSize" ID="TitleSubject" runat="server" Text="PRN292 Fall 2019 - ChiLP"></asp:Label>

                                

                                        <div class="card mt-3">
                                            <div class="card-body">
                                                

                                                <asp:Repeater ID="Repeater1" runat="server">
                                                    <ItemTemplate>
                                                        <div class="card mt-2 p-l-2">

                                                            <div>
                                                                <a href="subject-each-week.aspx?subjectID=<%=SubjectID%>&section=<%#Eval("SectionID") %>">
                                                                    <asp:Label CssClass="topic-color medium-size" ID="Label1" runat="server" Text='<%# Eval("DateRange") %>'></asp:Label></a>
                                                            </div>
                                                            <div class="text-right small-size">
                                                                <div>File: <%#Eval("NumberOfFile") %></div>

                                                            </div>
                                                        </div>
                                                    </ItemTemplate>

                                                </asp:Repeater>







                                            </div>



                                        </div>
                                    </div>



                                </div>

                                <!-- /.container-fluid -->

                            </div>
                            <!-- End of Main Content -->

                            <!-- Footer -->

                            <!-- End of Footer -->

                        </div>
                        <!-- End of Content Wrapper -->


                        <!-- End of Page Wrapper -->

                        <!-- Scroll to Top Button-->
                        <a class="scroll-to-top rounded" href="#page-top">
                            <i class="fas fa-angle-up"></i>
                        </a>

                        <!-- Logout Modal-->
                        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                                    <div class="modal-footer">
                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                        <a class="btn btn-primary" href="login.html">Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Bootstrap core JavaScript-->
                        <script src="../../vendor/jquery/jquery.min.js"></script>
                        <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

                        <!-- Core plugin JavaScript-->
                        <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

                        <!-- Custom scripts for all pages-->
                        <script src="../../js/sb-admin-2.min.js"></script>
    </form>
</body>

</html>

