﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

namespace CMS_SE1310
{
    public partial class subject_each_week : System.Web.UI.Page
    {
        public string Prev { get; set; }

        public string Next { get; set; }

        public string SubjectID { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.Contents["Role"].ToString() == "Teacher")
            {
                btnUpload.Visible = true;
                fuSample.Visible = true;
            }
            try
            {
                if (Session.Contents["Name"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                lbName.Text = Session.Contents["Name"].ToString();
                if (!IsPostBack)
                {
                    DataProvider dp = new DataProvider();
                    int sectionid = Convert.ToInt32(Request.QueryString["section"]);
                    int subjectid = Convert.ToInt32(Request.QueryString["subjectID"]);
                    SubjectID = subjectid.ToString();
                    Label1.Text = GetDateRangeBySection(sectionid);
                    if (sectionid == 1)
                    {
                        Label3.Text = "";
                       
                        Label4.Text = GetDateRangeBySection(sectionid + 1) + " >";
                       
                        Next = Convert.ToString(sectionid + 1);
                    }
                    else if (sectionid == getMaxSection())
                    {
                        Label4.Text = "";
                      
                        Label3.Text = "< " + GetDateRangeBySection(sectionid - 1);
                        
                        Prev = Convert.ToString(sectionid - 1);
                    }
                    else
                    {
                        Label3.Text = "< " + GetDateRangeBySection(sectionid - 1);
                      
                        Label4.Text = GetDateRangeBySection(sectionid + 1) + " >";
                       
                        Prev = Convert.ToString(sectionid - 1);
                        Next = Convert.ToString(sectionid + 1);
                    }
                    Repeater1.DataSource = GetResourceBySection(sectionid, subjectid);
                    Repeater1.DataBind();
                    DataProvider data = new DataProvider();
                    TitleSubject.Text = data.GET_OBJECT("select SubjectName from Subject where SubjectID = @param1", new object[] { subjectid })["SubjectName"].ToString(); ;

                }
            }
            catch (Exception)
            {
                Response.Redirect("Error.aspx");
            }


        }



        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(fuSample.FileName))
            {

                btnUpload.OnClientClick += "return confirmDel()";
                return;
            }
            btnUpload.Attributes.Remove("OnClientClick");
            //Files is folder Name
            fuSample.SaveAs(Server.MapPath("Upload") + "//" + fuSample.FileName);
            //Add file name to database
            string fileName = fuSample.FileName.ToString();

            DataProvider p = new DataProvider();
            int sectionid = Convert.ToInt32(Request.QueryString["section"]);
            int subjectid = Convert.ToInt32(Request.QueryString["subjectID"]);

            int code = Convert.ToInt32(p.GET_OBJECT("select MAX(ResourceID) as [count] from [Subject_Resource]", null)["count"].ToString()) + 1;

            p.ADD_UPDATE_DELETE("insert into Subject_Resource values ( @p1 , @p2 , GETDATE(), @p3 , @p4 )", new object[] { code, fileName, subjectid, sectionid });
            Response.Redirect("subject-each-week.aspx?subjectID=" + subjectid + "&section=" + sectionid);
        }

        private List<Resource> GetResourceBySection(int sectionid, int subjectid)
        {
            DataProvider dp = new DataProvider();
            DataTable dt = dp.GET_LIST_OBJECT("select * from Subject_Resource where Section = @param1 and SubjectID = @param2", new object[] { sectionid, subjectid });

            List<Resource> listDate = new List<Resource>();
            foreach (DataRow dr in dt.Rows)
            {
                int resourceID = Convert.ToInt32(dr["ResourceID"]);
                string resourceName = (string)dr["ResourceName"];
                int subjectID = Convert.ToInt32(dr["SubjectID"]);
                listDate.Add(new Resource(resourceID, resourceName, subjectID));




            }
            return listDate;
        }
        private string GetDateRangeBySection(int sectionid)
        {
            DataProvider dp = new DataProvider();
            DataTable dt = dp.GET_LIST_OBJECT("select * from Section where section = @param1", new object[] { sectionid });


            foreach (DataRow dr in dt.Rows)
            {
                DateTime startdate = Convert.ToDateTime(dr["startdate"]);
                DateTime enddate = Convert.ToDateTime(dr["enddate"]);

                return startdate.ToString("dd MMMM") + " - " + enddate.ToString("dd MMMM");

            }
            return null;
        }
        private int getMaxSection()
        {
            DataProvider dp = new DataProvider();
            DataTable dt = dp.GET_LIST_OBJECT("select count(*) as [A] from Section", null);
            foreach (DataRow dr in dt.Rows)
            {
                int count = (int)dr["A"];
                return count;


            }
            return -1;
        }

    }
}