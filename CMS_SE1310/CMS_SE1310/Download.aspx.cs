﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_SE1310
{
    public partial class Download : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string fileName = Request.QueryString["FileName"].ToString();
            if (fileName != null)
            {
                if (fileName.EndsWith(".txt"))
                {
                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "text/plain";
                    response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ";");
                    response.TransmitFile(Server.MapPath("Upload") + "//" + fileName);
                    response.Flush();
                    response.End();
                }
                else if (fileName.EndsWith(".pdf"))
                {
                    System.Web.HttpResponse response2 = System.Web.HttpContext.Current.Response;
                    response2.ClearContent();
                    response2.Clear();
                    response2.ContentType = "application/pdf";
                    response2.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ";");
                    response2.TransmitFile(Server.MapPath("Upload") + "//" + fileName);
                    response2.Flush();
                    response2.End();
                }

            }


        }
    }
}