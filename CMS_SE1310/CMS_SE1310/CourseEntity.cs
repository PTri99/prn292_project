﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CMS_SE1310
{
    public class CourseEntity
    {
        public int CourseID { get; set; }
        public string CourseName { get; set; }
        public int CategoryID { get; set; }

        public List<CourseEntity> ConvertToListCourse(int categoryID)
        {
            DataProvider p = new DataProvider();
            DataTable tb = p.GET_LIST_OBJECT("select * from Course where CategoryID = @param1", new object[] { categoryID });
            List<CourseEntity> courseList = new List<CourseEntity>();

            foreach (DataRow item in tb.Rows)
            {
                CourseEntity c = new CourseEntity();
                c.CourseID = Convert.ToInt32(item["CourseID"]);
                c.CourseName = item["CourseName"].ToString();
                c.CategoryID = Convert.ToInt32(item["CategoryID"]);
                //Add to list
                courseList.Add(c);
            }
            return courseList;
        }

    }
}