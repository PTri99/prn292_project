﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_SE1310
{
    public partial class HomeALogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Kiểm tra đã login chưa
                if (Session.Contents["Name"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    lbName.Text = Session.Contents["Name"].ToString();
                    Category c = new Category();
                    Repeater1.DataSource = c.ConvertToListCat();
                    Repeater1.DataBind();
                }

            }
            catch (Exception)
            {
                Response.Redirect("Error.aspx");
            }
        }

        protected void btnSubmitSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string keyword = tbSearch.Text;
                Response.Redirect("SearchResult.aspx?keyword=" + keyword);
            }
            catch (Exception)
            {
                Response.Redirect("Error.aspx");
            }
        }
    }
}