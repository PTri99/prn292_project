﻿

function showPopUp() {
    document.getElementById('id01').style.display = 'block';
}
function test() {
    alert("Changed");
}
function verifyInput() {
    
    
    var name = document.getElementById("nameuser").value;
    if (name === "") {
        alert("Name must no empty");
        return false;
    }
    var email = document.getElementById("emailuser").value;
    
    if (email === "") {
        alert("Email must no empty");
       return false;
    }


    var isEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    if (isEmail === false) {

        alert("Email is not valid");
        return  false;
    }

    var dob = document.getElementById("dobuser").value;
    var isDOB = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(dob);
    if (isDOB === false) {
        alert("DOB is not valid");
        return false;
    }

    return true;



}
