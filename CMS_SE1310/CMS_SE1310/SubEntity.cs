﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace CMS_SE1310
{
    public class SubEntity
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public static List<SubEntity> GetListSubjectByCourseID(int courseID)
        {
            DataProvider dp = new DataProvider();
            string query = "select SubjectID, SubjectName, StartDate, EndDate from [subject] s, course c where c.CourseID = s.CourseID and c.CourseID = @param1";
            DataTable dt = dp.GET_LIST_OBJECT(query, new object[] { courseID });
            List<SubEntity> list = new List<SubEntity>();

            foreach(DataRow item in dt.Rows)
            {
                SubEntity se = new SubEntity();
                se.SubjectID = Convert.ToInt32(item["SubjectID"]);
                se.SubjectName = item["SubjectName"].ToString();
                se.StartDate = Convert.ToDateTime(item["startdate"]);
                se.EndDate = Convert.ToDateTime(item["enddate"]);
                list.Add(se);
            }
            return list;
        }
    }
}