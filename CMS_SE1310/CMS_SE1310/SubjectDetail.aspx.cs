﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_SE1310
{
    public partial class SubjectDetail : System.Web.UI.Page
    {
        public string SubjectID { get; set; }

        public void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session.Contents["Name"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                lbName.Text = Session.Contents["Name"].ToString();
                if (!IsPostBack)
                {


                }
                string id = Request.QueryString["id"];
                SubjectID = id;
                Repeater1.DataSource = GetAllTimeRange();
                Repeater1.DataBind();

                DataProvider data = new DataProvider();
                TitleSubject.Text = data.GET_OBJECT("select SubjectName from Subject where SubjectID = @param1", new object[] { id })["SubjectName"].ToString();
            }
            catch (Exception)
            {
                Response.Redirect("Error.aspx");
            }
        }
       
        public  List<Section> GetAllTimeRange()
        {
            DataProvider dp = new DataProvider();
            DataTable dt = dp.GET_LIST_OBJECT("select * from Section",null);

            List<Section> listDate = new List<Section>();
            foreach (DataRow dr in dt.Rows)
            {
                int sectionID = Convert.ToInt32(dr["section"]);
                DateTime startdate = Convert.ToDateTime(dr["startdate"]);
                DateTime enddate = Convert.ToDateTime(dr["enddate"]);

                listDate.Add(new Section(sectionID, startdate.ToString("dd MMMM") + " - " + enddate.ToString("dd MMMM"),getNumOfFileBySection(sectionID, Convert.ToInt32(SubjectID))));
               



            }
            return listDate;
        }
        private int getNumOfFileBySection(int sectionid, int subjectid )
        {
            DataProvider dp = new DataProvider();
            DataTable dt = dp.GET_LIST_OBJECT("select count(*) as [A] from Subject_Resource where section = @param1 and SubjectID = @param2 ", new object[] {sectionid, subjectid});


            foreach (DataRow dr in dt.Rows)
            {

                int count = (int)dr["A"];
                return count;
            }
            return -1;
        }
    }
}