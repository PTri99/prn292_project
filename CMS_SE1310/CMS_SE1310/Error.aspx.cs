﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_SE1310
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.Contents["Name"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                lbName.Text = Session.Contents["Name"].ToString();
            }
        }
    }
}