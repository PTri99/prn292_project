﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS_SE1310.App_Code.Entity
{
    public class User
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public User(string name, string sex, string dob, string email, string rolename)
        {
         
            Name = name;
            Sex = sex;
            DOB = dob;
            Email = email;
            RoleName = rolename;
        }
        public User(int userID, string name, string sex, string dob, string email, string rolename )
        {
            UserID = userID;
            Name = name;
            Sex = sex;
            DOB = dob;
            Email = email;
            RoleName = rolename;
        }
    }
}