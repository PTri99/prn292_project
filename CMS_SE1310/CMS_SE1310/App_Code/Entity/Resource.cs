﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS_SE1310
{
    class Resource
    {
        public int ResourceID { get; set; }
        public string ResourceName { get; set; }
        public int SubjectID { get; set; }
        

        public Resource(int resourceID, string resourceName, int subjectID)
        {
            ResourceID = resourceID;
            ResourceName = resourceName;
            SubjectID = subjectID;
           
        }
    }
}