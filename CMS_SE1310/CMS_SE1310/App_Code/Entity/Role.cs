﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS_SE1310.App_Code.Entity
{
    public class Role
    {
        public int RoleID { get; set; }
        public string Name { get; set; }
        public Role(int roleID, string name)
        {
            RoleID = roleID;
            Name = name;
        }
    }
}