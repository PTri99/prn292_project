﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS_SE1310
{
    public class Section
    {

        public int SectionID { get; set; }
        public string DateRange { get; set; }
        public int NumberOfFile { get; set; }
        public Section(int sectionID, string dateRange, int numberfile)
        {
            SectionID = sectionID;
            DateRange = dateRange;
            NumberOfFile = numberfile;
        }
    }
}