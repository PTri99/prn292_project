﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS_SE1310.App_Code.Entity
{
    public class Subject
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public int CourseID { get; set; }
        public Subject(int subjectID, string subjectName, int courseID)
        {
            SubjectID = subjectID;
            SubjectName = subjectName;
            CourseID = courseID;
        }
    }
}