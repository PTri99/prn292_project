﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin-List-User.aspx.cs" Inherits="CMS_SE1310.Admin_List_User" %>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Manage user</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="CSS/CustomCSS/Admin-List-User.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
</head>

<body id="page-top">
    <form id="form1" runat="server">
        <!-- Page Wrapper -->
        <div id="wrapper">
           
            <!-- Sidebar -->
            
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-light bg-white topbar mb-4 static-top shadow">
                        <a href="HomeALogin.aspx">
                            <h1 class="title">eLearning</h1>
                        </a>
                        <div class="float-right">
                            You are signed in as
                            <asp:Label ID="lbName" runat="server" Text="Label1"></asp:Label>
                            <a href="SignOut.aspx" class="btn btn-primary btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-arrow-right"></i>
                                </span>
                                <span class="text">Sign Out</span>
                            </a>
                        </div>
                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">List of user</h1>

                        <div class="">
                            <asp:DropDownList ID="DropDownList1" AutoPostBack="true" CssClass="btn btn-primary dropdown-toggle float-left mr-5" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <button onclick="document.getElementById('id01').style.display='block'" style="width: auto;" class="btn btn-primary btn-icon-split addBot  mr-5" type="button">Add user</button>
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridViewUser" AutoGenerateColumns="false" CssClass="table table-bordered" Width="100%" CellPadding="0" runat="server" OnRowDataBound="GridViewUser_RowDataBound">

                                        <%--<Columns>
                                            <asp:BoundField DataField="UserID" HeaderText="ID" />
                                            <asp:BoundField DataField="Name" HeaderText="Name" />
                                            <asp:BoundField DataField="Sex" HeaderText="Sex" />
                                            <asp:BoundField DataField="DOB" HeaderText="DOB" />
                                            <asp:BoundField DataField="Email" HeaderText="Email" />
                                            <asp:BoundField DataField="RoleName" HeaderText="RoleName" />
                                            <asp:TemplateField HeaderText="Change role">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DropDownList2" runat="server">
                                                        
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                        </Columns>--%>

                                    </asp:GridView>

                                </div>
                                <asp:Button CssClass="btn btn-primary btn-icon-split cusBot" ID="Save" runat="server" Text="Save" OnClick="Save_Click" CausesValidation="false"/>

                                <div id="id01" class="modal">

                                    <div class="modal-content animate">
                                        <div class="imgcontainer">
                                            <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>

                                        </div>

                                        <div class="container">
                                           
                                            <label><b>Name</b></label>
                                            <asp:TextBox ID="nameuser" runat="server" placeholder="Name" CssClass="mb-4" ></asp:TextBox>

                                            <asp:RadioButton GroupName="sex" ID="RadioButton1" runat="server" Checked="True" />
                                            Male
                                            <asp:RadioButton CssClass="ml-5" GroupName="sex" ID="RadioButton2" runat="server" />
                                            Female
                                            <br />
                                            <label class="mt-4"><b>DOB</b></label>
                                            <asp:TextBox ID="dobuser" runat="server" placeholder="DOB"  CssClass="mb-4"></asp:TextBox>

                                            

                                            <label><b>Email</b></label>
                                            <asp:TextBox ID="emailuser" runat="server" placeholder="Email"  CssClass="mb-4"></asp:TextBox>

                                            <label><b>Role</b></label>
                                            <asp:DropDownList ID="DropDownList3" runat="server" CssClass="mb-4"></asp:DropDownList>
                                            <asp:Button CssClass="customAddBt" ID="Add" runat="server" Text="Add" OnClientClick="return verifyInput();" OnClick="Add_Click" />
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Custom JavaScript-->
        <script src="js/CustomJS/Admin-List-User.js"></script>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="js/demo/datatables-demo.js"></script>
    </form>
</body>
<script>
    function confirmDel() {
        return alert('Please choose file again');
    }
    </script>
</html>

