﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace CMS_SE1310
{
    public class SearchObject
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public string CategoryName { get; set; }
        public string TeacherName { get; set; }

        public static List<SearchObject> ListSearch(string keyword)
        {
            DataProvider dp = new DataProvider();
            string query = "select s.SubjectID, s.SubjectName, cat.CategoryName, u.Name from subject s, [user] u, User_Subject us, course c, Categories cat  where u.UserID = us.UserID and s.SubjectID = us.SubjectID and s.CourseID = c.CourseID and c.CategoryID = cat.CategoryID  and s.SubjectName like @param1";
            keyword = "%" + keyword + "%";
            DataTable dt = dp.GET_LIST_OBJECT(query, new object[] { keyword });
            List<SearchObject> list = new List<SearchObject>();
            foreach(DataRow item in dt.Rows)
            {
                SearchObject so = new SearchObject();
                so.SubjectID = Convert.ToInt32(item["SubjectID"]);
                so.SubjectName = item["SubjectName"].ToString();
                so.CategoryName = item["CategoryName"].ToString();
                so.TeacherName = item["Name"].ToString();
                list.Add(so);
            }
            return list;
        }
    }
}