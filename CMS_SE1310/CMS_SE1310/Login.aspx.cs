﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_SE1310
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Nếu người dùng đã log-in, không cho vào trang login
                if (Session.Contents["Name"] != null)
                {
                    Response.Redirect("HomeALogin.aspx");
                }
                //Set error text to empty
                errLogin.Text = "";
            }
            catch (Exception)
            {
                Response.Redirect("Error.aspx");
            }
        }


        //Khi người dùng bấm nút Login
        protected void Button1_Click1(object sender, EventArgs e)
        {
            try
            {
                //Lấy user / password người dùng vừa nhập
                String userName = tbUserName.Text;
                String password = tbPassword.Text;
                DataProvider data = new DataProvider();
                object user = data.GET_OBJECT("select * from Account where email = @param1 and Password = @param2", new object[] { userName, password });

                if (user != null)
                {
                    string role = data.GET_OBJECT("select RoleName from Role, Account, [User] where [Role].RoleID = [User].RoleID and [User].UserID = Account.UserID and [Account].Email = @param1", new object[] { userName })["RoleName"].ToString();
                    // chuyen den trang hoc sinh
                    if (role.Equals("Student"))
                    {
                        Session.Add("Name", userName);
                        Session.Add("Role", "Student");
                        Response.Redirect("HomeALogin.aspx");
                    }
                    // chuyen den trang admin
                    else if (role.Equals("Admin"))
                    {

                        Session.Add("Name", userName);
                        Session.Add("Role", "Admin");
                        Response.Redirect("Admin-List-User.aspx");
                    }
                    //chuyen den trang teacher
                    else
                    {
                        Session.Add("Name", userName);
                        Session.Add("Role", "Teacher");
                        Response.Redirect("HomeALogin.aspx");
                    }

                }
                else
                {
                    errLogin.Text = "Wrong account or password";
                }


            }
            catch (Exception)
            {
                Response.Redirect("Error.aspx");
            }
        }
    }
}