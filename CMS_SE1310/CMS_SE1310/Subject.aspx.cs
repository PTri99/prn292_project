﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_SE1310
{
    public partial class Subject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session.Contents["Name"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    lbName.Text = Session.Contents["Name"].ToString();
                    int courseID = Convert.ToInt32(Request.QueryString["id"]);
                    List<SubEntity> list = SubEntity.GetListSubjectByCourseID(courseID);
                    Repeater1.DataSource = list;
                    Repeater1.DataBind();
                }
            }
            catch (Exception)
            {
                Response.Redirect("Error.aspx");
            }
        }
    }
}