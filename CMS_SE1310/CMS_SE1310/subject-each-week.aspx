﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="subject-each-week.aspx.cs" Inherits="CMS_SE1310.subject_each_week" %>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resource week</title>
    <link href="../../CSS/boostrap_4/bootstrap.scss" rel="stylesheet" type="text/css">
    <link href="../../CSS/CustomCSS/SubjectDetail.css" rel="stylesheet" type="text/css">
    <link href="CSS/CustomCSS/subject-each-week.css" rel="stylesheet" type="text/css">

    <!-- Custom fonts for this template-->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="CSS/boostrap_4/bootstrap-grid.scss" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../../CSS/sb-admin-2.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <form id="form1" runat="server">

        <!-- Page Wrapper -->

        <div id="wrapper">


            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Topbar -->
                <nav class="navbar navbar-light bg-white topbar mb-4 static-top shadow">
                    <a href="HomeALogin.aspx">
                        <h1 class="title">eLearning</h1>
                    </a>
                    <div class="float-right">
                        You are signed in as:
                            <asp:Label ID="lbName" runat="server" Text="Label"></asp:Label>
                        <a href="SignOut.aspx" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-arrow-right"></i>
                            </span>
                            <span class="text">Sign Out</span>
                        </a>
                    </div>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label CssClass="font-weight-bold titleSize" ID="TitleSubject" runat="server" Text="PRN292 Fall 2019 - ChiLP"></asp:Label>

                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-body ml-5">
                            <div>
                                <img class="d-inline-block" src="Img/genetal.JPG" />
                                <p class="d-inline-block topic-color">Announcements Forum</p>
                            </div>
                            <div>
                                <img class="d-inline-block" src="Img/ppt.JPG" />
                                <p class="d-inline-block topic-color">Slide (all)</p>
                            </div>

                            <%-- Button upload --%>
                            <%--<div class="text-right">
                                <asp:Button CssClass="btn btn-secondary btn-icon-split" ID="btnUpload" runat="server" Text="Upload file" Height="38px" Width="137px" Visible="false">                           
                                </asp:Button>
                            </div>--%>

                            <div class="text-right">
                            <asp:FileUpload runat="server" ID="fuSample" Visible="false" />
                            <asp:Button runat="server" ID="btnUpload" Text="Upload"
                                OnClick="btnUpload_Click" CssClass="btn btn-secondary btn-icon-split" Height="38px" Width="137px" Visible="false"/>
                            </div>

                            <%-- End button upload --%>


                            <div class="clearfix mt-3">

                                <a href="subject-each-week.aspx?subjectID=<%=SubjectID%>&section=<%=Prev%>">
                                    <asp:Label ID="Label3" CssClass="float-left size-120 topic-color" runat="server" Text="9 September - 15 September"></asp:Label></a>

                                <a href="subject-each-week.aspx?subjectID=<%=SubjectID%>&section=<%=Next%>">
                                    <asp:Label ID="Label4" CssClass="float-right size-120 topic-color" runat="server" Text="23 September - 29 September"></asp:Label></a>

                            </div>
                            <div class="text-center">
                                <asp:Label ID="Label1" CssClass="size-150" runat="server" Text="16 September - 22 September"></asp:Label>
                            </div>
                            <!--day la content-->
                            <asp:Repeater ID="Repeater1" runat="server">
                                <ItemTemplate>
                                    <div class="content-resources ml-5 mb-2">
                                        <img class="d-inline-block" src="Img/ppt.JPG" />
                                        <a href="Download.aspx?FileName=<%# Eval("ResourceName") %>">
                                            <asp:Label ID="Label2" CssClass="d-inline-block" runat="server" Text='<%# Eval("ResourceName") %>'></asp:Label></a>
                                    </div>
                                </ItemTemplate>

                            </asp:Repeater>

                            <!--ket thuc content-->
                            







                        </div>



                    </div>
                </div>



            </div>

            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->

        <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

        </div>
  <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="../../vendor/jquery/jquery.min.js"></script>
        <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="../../js/sb-admin-2.min.js"></script>
    </form>
</body>
     <script>
         function confirmDel() {
             return alert('Please choose file again');
        }
    </script>
</html>


