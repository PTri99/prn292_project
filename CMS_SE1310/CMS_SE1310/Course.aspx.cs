﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_SE1310
{
    public partial class Course : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session.Contents["Name"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    lbName.Text = Session.Contents["Name"].ToString();
                    Category c = new Category();
                    Repeater1.DataSource = c.ConvertToListCat();
                    Repeater1.DataBind();
                    if (Request.QueryString["ID"] != null)
                    {
                        int id = Convert.ToInt32(Request.QueryString["ID"]);
                        CourseEntity course = new CourseEntity();
                        Repeater2.DataSource = course.ConvertToListCourse(id);
                        Repeater2.DataBind();
                    }
                }
            }
            catch (Exception)
            {
                Response.Redirect("Error.aspx");
            }
        }
    }
}