﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CMS_SE1310
{
    public partial class SearchResult : System.Web.UI.Page
    {
        DataProvider dp = new DataProvider();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session.Contents["Name"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                lbName.Text = Session.Contents["Name"].ToString();
                string keyword = Request.QueryString["keyword"];
                List<SearchObject> listSearch = SearchObject.ListSearch(keyword);
                lbCount.Text = listSearch.Count.ToString();
                Repeater1.DataSource = listSearch;
                Repeater1.DataBind();
            }
            catch (Exception)
            {
                Response.Redirect("Error.aspx");
            }
        }

        protected void btnSubmitSearch_Click(object sender, EventArgs e)
        {
            string keyword = tbSearch.Text;
            Response.Redirect("SearchResult.aspx?keyword=" + keyword);
        }
    }
}